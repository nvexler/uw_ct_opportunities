<?php
/**
 * @file
 * uw_ct_opportunities.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uw_ct_opportunities_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'uw_opportunities-front_page';
  $context->description = '';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'uw_ct_opportunities-front_page' => array(
          'module' => 'uw_ct_opportunities',
          'delta' => 'front_page',
          'region' => 'content',
          'weight' => '7',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  $export['uw_opportunities-front_page'] = $context;

  return $export;
}
