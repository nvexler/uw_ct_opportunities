<?php
/**
 * @file
 * uw_ct_opportunities.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_opportunities_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create uw_opportunities content'.
  $permissions['create uw_opportunities content'] = array(
    'name' => 'create uw_opportunities content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any uw_opportunities content'.
  $permissions['delete any uw_opportunities content'] = array(
    'name' => 'delete any uw_opportunities content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own uw_opportunities content'.
  $permissions['delete own uw_opportunities content'] = array(
    'name' => 'delete own uw_opportunities content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any uw_opportunities content'.
  $permissions['edit any uw_opportunities content'] = array(
    'name' => 'edit any uw_opportunities content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own uw_opportunities content'.
  $permissions['edit own uw_opportunities content'] = array(
    'name' => 'edit own uw_opportunities content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'enter uw_opportunities revision log entry'.
  $permissions['enter uw_opportunities revision log entry'] = array(
    'name' => 'enter uw_opportunities revision log entry',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_opportunities authored by option'.
  $permissions['override uw_opportunities authored by option'] = array(
    'name' => 'override uw_opportunities authored by option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_opportunities authored on option'.
  $permissions['override uw_opportunities authored on option'] = array(
    'name' => 'override uw_opportunities authored on option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_opportunities promote to front page option'.
  $permissions['override uw_opportunities promote to front page option'] = array(
    'name' => 'override uw_opportunities promote to front page option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_opportunities published option'.
  $permissions['override uw_opportunities published option'] = array(
    'name' => 'override uw_opportunities published option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_opportunities revision option'.
  $permissions['override uw_opportunities revision option'] = array(
    'name' => 'override uw_opportunities revision option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_opportunities sticky option'.
  $permissions['override uw_opportunities sticky option'] = array(
    'name' => 'override uw_opportunities sticky option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'search uw_opportunities content'.
  $permissions['search uw_opportunities content'] = array(
    'name' => 'search uw_opportunities content',
    'roles' => array(),
    'module' => 'search_config',
  );

  return $permissions;
}
