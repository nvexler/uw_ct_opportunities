<?php
/**
 * @file
 * uw_ct_opportunities.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_ct_opportunities_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "services" && $api == "services") {
    return array("version" => "3");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function uw_ct_opportunities_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function uw_ct_opportunities_node_info() {
  $items = array(
    'uw_opportunities' => array(
      'name' => t('Opportunities'),
      'base' => 'node_content',
      'description' => t('A content type used to display job/volunteer opportunities.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
